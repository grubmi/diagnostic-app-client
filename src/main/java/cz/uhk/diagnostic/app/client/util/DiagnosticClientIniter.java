package cz.uhk.diagnostic.app.client.util;

import cz.uhk.diagnostic.app.client.service.DiagnosticClient;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;

public class DiagnosticClientIniter extends ChannelInitializer<SocketChannel> {

    private final SslContext sslContext;
    private final DiagnosticClient diagnosticClient = new DiagnosticClient();

    public DiagnosticClientIniter(SslContext sslContext) {
        this.sslContext = sslContext;
    }

    @Override
    protected void initChannel(SocketChannel channel) {
        ChannelPipeline pipeline = channel.pipeline();
        //Order important!!
        pipeline.addLast(sslContext.newHandler(channel.alloc(), diagnosticClient.getHost(), diagnosticClient.getPort()));
        pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pipeline.addLast(new StringDecoder());
        pipeline.addLast(new StringEncoder());
        pipeline.addLast(new DiagnosticClientHandler());
    }
}
