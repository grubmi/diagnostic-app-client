package cz.uhk.diagnostic.app.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class DiagnosticClientApp {
    public static void main(String[] args) {
        SpringApplication.run(DiagnosticClientApp.class, args);
    }
}
