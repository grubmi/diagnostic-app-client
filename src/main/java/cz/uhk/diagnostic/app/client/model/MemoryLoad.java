package cz.uhk.diagnostic.app.client.model;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.sql.Timestamp;

public class MemoryLoad {
    private Timestamp timestamp;
    private double initMemory;
    private double usedMemory;
    private double maxMemory;
    private double commitMemory;

    //1073741824 bytes = 1 GB
    private static final int convert = 1073741824;

    //Measure JVM memory
    public void startMeasure() {
        MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
        setInitMemory((double) memoryMXBean.getHeapMemoryUsage().getInit() / convert);
        setMaxMemory((double) memoryMXBean.getHeapMemoryUsage().getMax() / convert);
        setUsedMemory((double) memoryMXBean.getHeapMemoryUsage().getUsed() / convert);
        setCommitMemory((double) memoryMXBean.getHeapMemoryUsage().getCommitted() / convert);
        setTimestamp();
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp() {
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }

    public double getInitMemory() {
        return initMemory;
    }

    public void setInitMemory(double initMemory) {
        this.initMemory = initMemory;
    }

    public double getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(double usedMemory) {
        this.usedMemory = usedMemory;
    }

    public double getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(double maxMemory) {
        this.maxMemory = maxMemory;
    }

    public double getCommitMemory() {
        return commitMemory;
    }

    public void setCommitMemory(double commitMemory) {
        this.commitMemory = commitMemory;
    }
}
