package cz.uhk.diagnostic.app.client.service;

import com.google.gson.Gson;
import cz.uhk.diagnostic.app.client.model.MemoryLoad;
import cz.uhk.diagnostic.app.client.util.DiagnosticClientIniter;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class DiagnosticClient {
    private static final String HOST = System.getProperty("host", "127.0.0.1");
    private static final int PORT = Integer.parseInt(System.getProperty("port", "8442"));

    private ChannelFuture lastWriteFuture = null;
    private Channel channel = null;
    private boolean endSending = false; //contact server to disconnect - end on server side
    private boolean cancelLoop = false; //disconnect on client - end on client side
    private final MemoryLoad memoryLoad = new MemoryLoad();

    //Autostart after spring boot
    @EventListener(ApplicationReadyEvent.class)
    public void initClient() throws Exception {
        // Configure SSL context.
        final SslContext sslContext = SslContextBuilder.forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE).build();

        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new DiagnosticClientIniter(sslContext));

            // Connection attempt.
            channel = b.connect(HOST, PORT).sync().channel();

            for (; ; ) {
                //Loop until endChannel = false
                //When sending message can change boolean to end communication
                if (isCancelLoop()) {
                    break;
                }
            }

            // Wait until all messages are flushed before closing the channel.
            if (lastWriteFuture != null) {
                lastWriteFuture.sync();
            }
        } finally {
            // Connection closed automatically on shutdown.
            group.shutdownGracefully();
        }
    }

    @Scheduled(fixedRate = 1000)
    public void autoSender() {
        memoryLoad.startMeasure();
        sendMsg(memoryLoad, isEndSending());
    }

    public void sendMsg(MemoryLoad load, Boolean end) {
        setEndSending(end);
        if (channel != null) {
            //if setEndSending is true, send empty load to cancel connection
            if (isEndSending()) {
                lastWriteFuture = channel.writeAndFlush("end" + "\r\n");
            } else {
                lastWriteFuture = channel.writeAndFlush(loadToJson(load) + "\r\n");
            }
        }
    }

    public boolean isEndSending() {
        return endSending;
    }

    public void setEndSending(boolean endSending) {
        this.endSending = endSending;
    }

    public String getHost() {
        return HOST;
    }

    public int getPort() {
        return PORT;
    }

    public boolean isCancelLoop() {
        return cancelLoop;
    }

    public void setCancelLoop(boolean cancelLoop) {
        this.cancelLoop = cancelLoop;
    }

    public String loadToJson(MemoryLoad load) {
        Gson gson = new Gson();
        return gson.toJson(load);
    }
}
